@extends('layouts.default')

@section('title')

<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Keyword List</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">View Keywords</a></li>
              <li class="breadcrumb-item active">Keyword List</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
 @endsection     

@section('content')

  <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <!-- <h3 class="card-title"></h3> -->
              <div class="btn-group">
                
                <div class="btn-group">
                  <a href="#" class="btn btn-info btn-app" id="create-new-keyword" data-toggle="modal" data-target="#modal-default">
                    <i class="fas fa-plus"></i> Create New Keyword
                  </a>
                  <a class="btn btn-light btn-app">
                    <i class="fas fa-sync"></i> Refresh
                  </a>

                </div>

              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="table-responsive">
                <table id="example2" class="table table-bordered table-hover data-table">
                  <thead>
                  <tr>
                    <th>Keyword</th>
                    <th>Product</th>
                    <th>Date Created</th>
                    <th>Actions</th>
                  </tr>
                  </thead>
                  <tbody>

                  </tbody>
                  <tfoot>
                  <tr>
                    <th>Keyword</th>
                    <th>Product</th>
                    <th>Date Created</th>
                    <th>Actions</th>
                  </tr>
                  </tfoot>
                </table>
                
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

        </div>
        <!-- /.col -->
        <div class="modal fade" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-keyword">

          <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="modal-title">Add New Keyword</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
               <form id="keywordForm" name="keywordForm">
                @csrf
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    
                    <div class="form-group">
                      <input type="hidden" class="form-control" name="keyword_id_" id="keyword_id" value="">
                      <label for="product_id_">Product</label>
                      <select name="product_id" id="product_id_" class="form-control custom-select-sm">
                        <option selected>Select Product</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="keyword_name_">Keyword</label>
                      <input type="text" class="form-control" name="keyword_name" id="keyword_name_" placeholder="Enter keyword">
                    </div>
                  </div>
                </div>
                
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" id="btn-save" class="btn btn-primary">Submit</button>
            </div>
              </form>
          </div>
          </div>
        </div>
                 	
  </div>
<script>
  $(document).ready(function () {
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // When user click add  button.
    $('#create-new-keyword').click(function () {
        
        $.ajax({
          type: "GET",
          url: "{{ url('keyword') }}" +'/create',
          success: function (data) {

             //Populate selection box with product data (id and name).
             for (var i = data.length - 1; i >= 0; i--) {
                
                var _id = data[i]['id'];
                var _product = data[i]['product'];

                $('#product_id_').append("<option name='product_id' value='"+_id+"'>"+_product+"</option>");
             }

            $('#btn-save').val("create-keyword");
            $('#keywordForm').trigger("reset");
            $('#modal-title').html("Add New Keyword");
            $('#modal-keyword').modal('show');

          },
          error: function (data) {
              console.log('Error:', data);
          }
      });
    });

    //Table
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('keyword') }}",
        columns: [
            // {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'keyword', name: 'keyword'},
            {data: 'product', name: 'product'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    //Store
    $('#btn-save').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        // var selected_val = 
        var e = document.getElementById("product_id_");
        var selectedEquipmentDropdown = e.options[e.selectedIndex].value;//change it here

        console.log( $('#keywordForm').serialize() );
        $.ajax({
            data: $('#keywordForm').serialize(),
            url: "{{ route('keyword.store') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
              console.log(data);
       
                $('#keywordForm').trigger("reset");
                $('#modal-keyword').modal('hide');
                table.draw();
           
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btn-save').html('Save Changes');
            }
        });
    });

    // Edit.
    $('body').on('click', '#edit-keyword', function () {
        
        var keyword_id = $(this).data('id');
        
        $('#btn-save').html('Save Changes')
        
        $.get('keyword/' + keyword_id +'/edit', function (data) {

            var keyword_data = data.keyword;
            var products_data = data.product;
            
            console.log(keyword_data.product_id);
            
            $('#modal-title').html("Edit Keyword Details");
            $('#btn-save').val("edit-keyword");
            $('#modal-keyword').modal('show');
                        
           
            $('#product_id_').html("<option name'product_id' value='"+keyword_data.product_id+"'>"+keyword_data.product+"</option>");

            console.log("2 : " + keyword_data.product_id);

            var items = document.getElementById('#product_id_');
            var products_option= "";

            for (var i = products_data.length - 1; i >= 0; i--) {
               products_option +=  "<option name'product_id' value='"+products_data[i].id+"'>"+products_data[i].product+"</option>";
              
            }
            $('#product_id_').append(products_option);


            
            $('#keyword_name_').val(keyword_data.keyword);
            $('#keyword_id').val(keyword_data.id);
           
        })
     });

    //Delete button.
    $('body').on('click', '#delete-keyword', function () {
        
        var keyword_id = $(this).data("id");
        var token = "{{ csrf_token() }}";
        
        if(confirm("Are You sure want to delete? ")) {
          $.ajax({
              type: "DELETE",
              url: "{{ url('keyword') }}"+'/'+keyword_id,
              data: { "_token": token,
                      "id": keyword_id},
              success: function (data) {
                  $("#keyword_id_" + keyword_id).remove();
                  table.draw();
              },
              error: function (data) {
                  console.log('Error:', data);
              }
          });
        }
    });

   
  
  });
  
  
</script>

@stop