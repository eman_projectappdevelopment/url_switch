<!DOCTYPE html>
<html lang="en">
<head>

	@include('includes.footer-scripts')
	@include('includes.head')

</head>
	<body class="hold-transition sidebar-mini">
		<div class="wrapper">
			
			@include('includes.header')

			@include('includes.sidebar')

		<div class="content-wrapper">

			<section class="content-header">
				@yield('title')
			</section>

			<section class="content">
				@yield('content')
			</section>
      		
			
		</div>

		<!-- Main Footer -->
		<footer class="main-footer">
					@include('includes.footer')
				
		</footer>



		</div>
	</body>
</html>
