<!DOCTYPE html>
<html lang="en">
<head>

	@include('includes.head')

</head>
	<body class="hold-transition login-page">
		
				@yield('content')

		@include('includes.footer-scripts')

	</body>
</html>
