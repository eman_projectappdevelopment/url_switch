<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    //

    protected $fillable = [
        'id',
        'user_id',
        'product',
        'url',
        'slug',
        'asin',
        'department',
        'facebook_pixel',
        'keyword'       
    ];

}
