<!DOCTYPE html>
<html lang="en">
<head>

	@include('includes.head')

</head>
	<body class="hold-transition">
		@yield('content')


		@include('includes.footer-scripts')
	</body>
</html>
