<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\product;
use App\keyword;
use Redirect,Response;
use DataTables;
use DB;

class KeywordsController extends Controller
{
    //
    public function __construct() {
        
        $this->middleware('auth');
    }
    
    public function index( Request $request ) {

        if ($request->ajax()) {
            $data = DB::table('keywords')
            	->join('products', 'products.id', '=', 'keywords.product_id')
            	->select('products.id', 'products.product', 'keywords.id', 'keywords.keyword', 'keywords.created_at')
            	->get();
            
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                           $btn = '<a href="javascript:void(0)" id="edit-keyword" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit"  data-toggle="modal"data-target="#modal-default" class="edit btn btn-info mr-2 btn-sm editProduct"><span class="fas fa-pen"></span></a>';
   
                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip" id="delete-keyword"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm delete-keyword"><span class="fas fa-trash"></a>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('pages.front-end.keyword.keyword-view',compact('keyword'));
    }

    public function create() {
        
        $products = product::all();

        return Response::json($products);
    }    

    public function store(Request $request) {

    	$keyword = keyword::updateOrCreate(
            [ 'id' => $request->keyword_id_ ],
            ['keyword' => $request->keyword_name,
                'product_id' => $request->product_id ]
        );
        
        return Response::json($keyword);
        
    }

    public function edit( $id ) {   
        
        $where = array('keywords.id' => $id);
        // $keyword  = keyword::where($where)->first();

        $keyword = DB::table('keywords')
            	->join('products', 'products.id', '=', 'keywords.product_id')
            	->select('products.id', 'products.product', 'keywords.id', 'keywords.keyword', 'keywords.created_at', 'keywords.product_id')
            	->where($where)->first();

        $product = product::all();

        $response = [
        	'keyword'=> $keyword,
        	'product' => $product
        ];
 
        return Response::json($response);
    }

    public function destroy( $id ) {
        
        $keyword = keyword::find($id);
        $keyword->delete();
        return Response::json($keyword);
    }
}
