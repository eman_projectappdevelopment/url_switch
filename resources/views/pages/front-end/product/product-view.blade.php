@extends('layouts.default')

@section('title')

<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Product List</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">View Products</a></li>
              <li class="breadcrumb-item active">Product List</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
 @endsection     

@section('content')

  <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <div class="col-md-12">
                <div class="row">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                </div>
                <div class="row">
                  <div class="btn-group">
                    <div class="btn-group">
                      <a href="{{route('product.create')}}" class="btn btn-info btn-app" id="create-new-product" data-toggle="modal"data-target="#modal-default">
                        <i class="fas fa-plus"></i> Create New Product
                      </a>
                      <a  href="{{ url('product') }}" class="btn btn-light btn-app">
                        <i class="fas fa-sync"></i> Refresh
                      </a>

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="table-responsive">
                <table id="example2" class="table table-bordered table-hover data-table">
                  <thead>
                  <tr>
                    <th>Product</th>
                    <th>Asin</th>
                    <th>Slug</th>
                    <th>Keyword</th>
                    <th>Date Created</th>
                    <th>Actions</th>
                  </tr>
                  </thead>
                  <tbody>
                    

                  </tbody>
                  <tfoot>
                    <th>Product</th>
                    <th>Asin</th>
                    <th>Slug</th>
                    <th>Keyword</th>
                    <th>Date Created</th>
                    <th>Actions</th>
                  </tfoot>
                </table>
                
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

        </div>
        <!-- /.col -->

        <div class="modal fade" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-product">

          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="modal-title">Add New Product Url </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="productForm" name="productForm">
                  @csrf
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="hidden" class="form-control" name="product_id_" id="product_id" value="">
                          <input type="hidden" class="form-control" name="user_id_" id="user_id" value="">
                          <label for="product_name">Product Name</label>
                          <input type="text" class="form-control" name="product_name_" id="product_name" placeholder="Enter Product Name">
                        </div>
                        <div class="form-group">
                          <label for="product_asin">ASIN</label>
                          <input type="text" class="form-control" name="product_asin_" id="product_asin" placeholder="Enter Product ASIN">
                        </div>
                        <div class="form-group">
                          <label for="product_department">Deparment</label>
                          <select class="form-control select2" name="product_department_" id="product_department" style="width: 100%;">
                            <option selected="selected">All departments</option>
                            <option>Alaska</option>
                            <option>California</option>
                            <option>Delaware</option>
                            <option>Tennessee</option>
                            <option>Texas</option>
                            <option>Washington</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="product_url">URL</label>
                          <input type="text" class="form-control" name="product_url_" id="product_url" placeholder="Enter Product URL">
                        </div>
                        <div class="form-group">
                          <label for="product_fb_pixel">Facebook Pixel</label>
                          <input type="text" class="form-control" name="product_fb_pixel_" id="product_fb_pixel" placeholder="Enter Product Facebook Pixel">
                        </div>
                        <div class="form-group">
                          <label for="product_slug">SLUG</label>
                          <input type="text" class="form-control" name="product_slug_" id="product_slug" placeholder="Enter Product SLUG">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="product_keyword">Keyword</label><br>
                          <select style="width: 100% !important;" multiple="true" type="text" class="form-control select2 col-md-12" name="product_keyword_[]" id="product_keyword" ></select>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="btn-save" value="create" class="btn btn-primary">Submit</button>
              </div>
              </form>
            </div>
          </div>
        </div>
  </div>


<script>
  $(document).ready(function () {
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // When user click add  button.
    $('#create-new-product').click(function () {
        
        $.ajax({
          type: "GET",
          url: "{{ url('product') }}" +'/create',
          success: function (data) {
            
            $('#btn-save').val("create-product");
            $('#productForm').trigger("reset");
            $('#user_id').val(data);
            $('#modal-title').html("Add New Product");
            $('#modal-product').modal('show');

          },
          error: function (data) {
              console.log('Error:', data);
          }
      });
    });

    // Edit.
    $('body').on('click', '#edit-product', function () {
        
        var product_id = $(this).data('id');
        
        $('#btn-save').html('Save Changes')
        
        $.get('product/' + product_id +'/edit', function (data) {
            
            $('#modal-title').html("Edit Product Information");
            $('#btn-save').val("edit-user");
            $('#modal-product').modal('show');
            
            //Keyword data
            var keywords = data.keyword;
            var arr = [];
            //Populate select2 with keywords
            for ( i = 0; i < keywords.length; i++ ) {
                arr.push(keywords[i]['keyword']);
            }

            $('.select2-search__field').val([arr]).trigger('change');

            //Product Data.
            $('#product_id').val(data.product.id);            
            $('#user_id').val(data.product.user_id);
            $('#product_name').val(data.product.product);
            $('#product_slug').val(data.product.slug);
            $('#product_department').val(data.product.department);
            $('#product_fb_pixel').val(data.product.facebook_pixel);
            $('#product_asin').val(data.product.asin);
            $('#product_url').val(data.product.url);
        })
     });

    //Delete button.
    $('body').on('click', '#delete-product', function () {
        
        var product_id = $(this).data("id");
        var token = "{{ csrf_token() }}";
        
        if(confirm("Are You sure want to delete !")) {
          $.ajax({
              type: "DELETE",
              url: "{{ url('product') }}"+'/'+product_id,
              data: { "_token": token,
                      "id": product_id},
              success: function (data) {
                  $("#product_id_" + product_id).remove();
                  table.draw();
              },
              error: function (data) {
                  console.log('Error:', data);
              }
          });
        }
    });

    //Table

    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('product') }}",
        columns: [
            // {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'product', name: 'product'},
            {data: 'asin', name: 'asin'},
            {data: 'slug', name: 'slug'},
            {data: 'keyword', name: 'keyword'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    $('#btn-save').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
    
      $.ajax({
          data: $('#productForm').serialize(),
          url: "{{ route('product.store') }}",
          type: "POST",
          dataType: 'json',
          success: function (data) {
     
              $('#productForm').trigger("reset");
              $('#modal-product').modal('hide');
              table.draw();
         
          },
          error: function (data) {
              console.log('Error:', data);
              $('#btn-save').html('Save Changes');
          }
      });
    });
  
  });


  $('#product_keyword').select2({
    tags: true,
    tokenSeparators: [",", " "]
  });

  
  
  
</script>
@stop

