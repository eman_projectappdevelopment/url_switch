<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');

/**
 * Login Route(s)
 */

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Auth::routes();

Route::get('dashboard', 'DashboardController@index')->name('dashboard')->middleware('auth');
Route::get('keyword', 'KeywordsController@index')->name('keyword')->middleware('auth');

//Product 
Route::get('product', 'ProductsController@index')->name('product')->middleware('auth');
Route::post('product/store', 'ProductsController@store')->name('product.store');
Route::get('product/create', 'ProductsController@create')->name('product.create');
Route::delete('product/{id}', 'ProductsController@destroy')->name('product.destroy');
Route::get('product/{product_id}/edit', 'ProductsController@edit')->name('product.edit');


//Keyword 
Route::get('keyword', 'KeywordsController@index')->name('keyword')->middleware('auth');
Route::post('keyword/store', 'KeywordsController@store')->name('keyword.store');
Route::get('keyword/create', 'KeywordsController@create')->name('keyword.create');
Route::delete('keyword/{id}', 'KeywordsController@destroy')->name('keyword.destroy');
Route::get('keyword/{keyword_id}/edit', 'KeywordsController@edit')->name('keyword.edit');
