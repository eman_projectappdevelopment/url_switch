<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script src="/bower_components/admin-lte/plugins/jquery/jquery.min.js"></script>

<script src="/bower_components/admin-lte/plugins/jquery-ui/jquery-ui.min.js"></script>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>


<!-- Bootstrap -->
<script src="/bower_components/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="/bower_components/admin-lte/dist/js/adminlte.js"></script>
<!-- overlayScrollbars -->
<script src="bower_components/admin-lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>

<script src="bower_components/admin-lte/plugins/select2/js/select2.full.min.js"></script>

<!-- DataTables -->
<script src="/bower_components/admin-lte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/bower_components/admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<!-- AdminLTE -->

<!-- Select2 -->
<script src="/bower_components/admin-lte/plugins/select2/js/select2.full.min.js"></script>


<!-- OPTIONAL SCRIPTS -->
<script src="/bower_components/admin-lte/plugins/chart.js/Chart.min.js"></script>
<script src="/bower_components/admin-lte/dist/js/demo.js"></script>
<script src="/bower_components/admin-lte/dist/js/pages/dashboard3.js"></script>


<!-- page script -->
