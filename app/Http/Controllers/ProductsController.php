<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;
use App\User;
use App\keyword;
use Redirect,Response;
use DataTables;

class ProductsController extends Controller
{
    //
    public function __construct() {
        
        $this->middleware('auth');
    }
    public function index( Request $request ) {

        if ($request->ajax()) {
            $data = product::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                           $btn = '<a href="javascript:void(0)" id="edit-product" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit"  data-toggle="modal"data-target="#modal-default" class="edit btn btn-info mr-2 btn-sm editProduct"><span class="fas fa-pen"></span></a>';
   
                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip" id="delete-product"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm delete-product"><span class="fas fa-trash"></a>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('pages.front-end.product.product-view',compact('product'));
    }


	public function create() {
        
        $user = auth()->user();
        $user_id = $user->id;

        return Response::json($user_id);
    }    

    public function store(Request $request) {

    	$user = auth()->user();
    	$user_id = $user->id;



    	$product = product::updateOrCreate(
            [ 'id' => $request->product_id_ ],
            ['user_id' => $request->user_id_,
                'product' => $request->product_name_,
            	'url' => $request->product_url_,
            	'slug' => $request->product_slug_,
            	'asin' => $request->product_asin_,
            	'department' => $request->product_department_,
            	'facebook_pixel' => $request->product_fb_pixel_,
                'keyword' => implode(',', $request->product_keyword_) ]
        );

        // Insert Keywords
        for ($i = 1; $i < count($request->product_keyword_); $i++) {
            $keyword = keyword::updateOrCreate(
                [ 'product_id' => $product->id,
                'keyword' => $request->product_keyword_[$i]
            ]);
        }
        
        return Response::json($product);
        
    }

    public function edit( $id ) {   
        
        $where_product_id = array('id' => $id);
        $where_keyword_product_id = array('product_id' => $id);
        
        $product  = product::where($where_product_id)->first();
        $keyword  = keyword::where($where_keyword_product_id)
            ->get()->toArray();

        $response = [
            'product' => $product,
            'keyword' => $keyword

        ];
 
        return Response::json($response);
    }

    public function destroy( $id ) {
        
        $product = product::find($id);
        $product->delete();
        return Response::json($product);
    }
}
